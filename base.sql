
CREATE TABLE public.autores
(
  id SERIAL,
  nombre character varying(250),
  CONSTRAINT pk_autor_id PRIMARY KEY (id)
);

CREATE TABLE public.estudiantes
(
  id SERIAL,
  dni character varying(30),
  nombre character varying(250),
  apellido character varying(250),
  CONSTRAINT pk_estudiante_id PRIMARY KEY (id)
);

CREATE TABLE public.libros
(
  id SERIAL,
  titulo character varying(250),
  editorial character varying(250),
  CONSTRAINT pk_libro_id PRIMARY KEY (id)
);

CREATE TABLE public.libros_autores
(
  id SERIAL,
  id_libros integer,
  id_autores integer,
  CONSTRAINT pk_libro_autor_id PRIMARY KEY (id),
  CONSTRAINT libroautor_autor FOREIGN KEY (id_autores)
      REFERENCES public.autores (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT libroautor_libro FOREIGN KEY (id_libros)
      REFERENCES public.libros (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE public.prestamos
(
  id SERIAL,
  id_estudiantes integer,
  id_libros integer,
  CONSTRAINT pk_prestamo_id PRIMARY KEY (id),
  CONSTRAINT fk_prestamo_estudiante FOREIGN KEY (id_estudiantes)
      REFERENCES public.estudiantes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_prestamo_libro FOREIGN KEY (id_libros)
      REFERENCES public.libros (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);